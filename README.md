0.1 First Learning ionic
    Penggunaan sistem operasi debian 10 Buster
    Mysql menggunakan Docker mariadb
    Ionic 5

    Berikut penjelasan ringkas
    sudo apt-get install python-software-properties
    $ curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
    $ sudo apt-get install -y nodejs
    $ sudo npm install -g cordova
    $ sudo npm install -g @ionic/cli

    ## Install docker
    $ sudo apt-get remove docker docker-engine docker.io containerd runc
    $ sudo apt-get update
    $ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common


    $ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
    $ sudo apt-key fingerprint 0EBFCD88
    $ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
    $ sudo apt-get update
    $ sudo apt-get install docker-ce docker-ce-cli containerd.io

    ## Install mariadb dan portainer
    $ sudo docker pull mariadb
    $ suder docker volume create mysql_data
    $ sudo docker run -d -p 3306:3306 --name mysql-server  -v mysql_data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=password -d mariadb:latest 
    $ sudo docker volume create portainer_data
    $ sudo docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
    CrudPegawai
    $ cd crudpegawai
    $ sudo npm install axios
