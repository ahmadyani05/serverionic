var express = require("express");
var bodyParser = require('body-parser');
var cors = require('cors');
var mysql = require('mysql');

var app = express();
app.use(cors());

var connection = mysql.createConnection({
	host 	  : 'localhost',
	user 	  : 'root',
	password  : 'password',
	database  : 'latihanionic'
})
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.get("/home", (req, res, next) => {
	res.json(["Selamat datang di API pegawai Kimia Farma"]);
	});

//fungsi get data seluruh pegawai
app.get('/pegawai/list', function (req, res) {
	connection.query('SELECT * FROM pegawai',
		function (error, pegawai, fields){
			if(error){console.log(error)} else{
				res.json({pegawai});
			}
		});
});
//fungsi create data pegawai
app.post("/pegawai/create", (req, res, next) => {
	var npp = req.body.npp;
	var nama = req.body.nama;
	var jabatan = req.body.jabatan;
	var sql = "INSERT INTO pegawai (npp, nama, jabatan) VALUES ('"+npp+"', '"+nama+"', '"+jabatan+"')";
	connection.query(sql, function (err, result) {
		if (err){
			res.json(["0"]);
		}else{
			console.log("1 record inserted");
			res.json(["1"]);
		}
	});
});

//fungsi hapus data pegawai
app.get('/pegawai/delete/:id', function (req, res) {
	var id = req.params.id;
	connection.query('DELETE FROM pegawai WHERE id = ?',[id],
		function (error, user, fields){
		if(error){
			console.log(error)
		} else{
			res.json(["1"]);
		}
	});
});

//fungsi pengubahan data pegawai
app.post('/pegawai/update', function (req, res) {
	var npp = req.body.id;
	var npp = req.body.npp;
	var nama = req.body.nama;
	var jabatan = req.body.jabatan;
	var sql = "UPDATE pegawai SET npp='"+npp+"', nama='"+nama+"',jabatan='"+jabatan+"', WHERE id='"+id+"'"; 
	connection.query(sql, function (error, user, fields){
		if(error){
			console.log(error)
		} else{
			res.json(["1"]);
		}
	});
});

app.listen(3000, () => {
	console.log("Server berjalan pada port 3000"); 
	});