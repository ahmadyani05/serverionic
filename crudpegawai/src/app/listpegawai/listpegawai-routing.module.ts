import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListpegawaiPage } from './listpegawai.page';

const routes: Routes = [
  {
    path: '',
    component: ListpegawaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListpegawaiPageRoutingModule {}
