import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListpegawaiPageRoutingModule } from './listpegawai-routing.module';

import { ListpegawaiPage } from './listpegawai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListpegawaiPageRoutingModule
  ],
  declarations: [ListpegawaiPage]
})
export class ListpegawaiPageModule {}
