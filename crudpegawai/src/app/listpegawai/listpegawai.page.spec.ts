import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListpegawaiPage } from './listpegawai.page';

describe('ListpegawaiPage', () => {
  let component: ListpegawaiPage;
  let fixture: ComponentFixture<ListpegawaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListpegawaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListpegawaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
