import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';

@Component({
  selector: 'app-listpegawai',
  templateUrl: './listpegawai.page.html',
  styleUrls: ['./listpegawai.page.scss'],
})

export class ListpegawaiPage implements OnInit {
  constructor(public routerCtrl: Router) { }
  dataPegawai : Array<object>;
  ngOnInit() {
    this.apiGetPegawai(); //memanggil method apiGetPegawai()
  }
  apiGetPegawai(){
    var self = this;
    var urlApi = 'http://localhost:3000/pegawai/list';
    axios.get(urlApi).then(function (response) {
      self.dataPegawai = response.data.pegawai;
    })
    .catch(function (error) {
      console.log(error);
    })
    .then(function () {
      });
    }
    apiEditPegawai(item){
      var data = item;
      this.routerCtrl.navigate(['editpegawai'],{
        queryParams: item
      });
    }
    apiDeletePegawai(item){
      var self = this;
      var urlApi ='http://localhost:3000/pegawai/delete/'+item.id;
      axios.get(urlApi)
      .then(function (response) {
        console.log(response);
        if(response.data[0] == '1'){}
      }).catch(function (error) {
        console.log(error);
      }).then(function () {
        location.reload(); //refresh page
      });
    }
  }